import { Injectable } from '@nestjs/common';

// This should be a real class/interface representing a user entity
export type User = any;

@Injectable()
export class UsersService {
    private readonly users = [
        {
            Id: 1,
            email: 'admin@email.com',
            password: 'Pass@1234',
        },
        {
            Id: 2,
            email: 'user1@email.com',
            password: 'Pass@1234',
        },
    ];

    async findOne(email: string): Promise<User | undefined> {
        return this.users.find(user => user.email === email);
    }
}